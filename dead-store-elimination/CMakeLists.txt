
add_llvm_library( LLVMOurDeadStoreElimination MODULE BUILDTREE_ONLY
  OurDeadStoreElimination.cpp

  DEPENDS
  intrinsics_gen
  PLUGIN_TOOL
  opt
  )
