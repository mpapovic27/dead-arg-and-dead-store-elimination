#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Value.h"


#include <iostream>
#include <vector>
#include <unordered_map>

using namespace llvm;

namespace{
struct OurDeadStoreElimination : public FunctionPass
{
    bool StoreRemoved;
    std::vector<Instruction *> StoreToRemove;
    std::unordered_map<Value * ,bool> StoreVariables;
    std::unordered_map<Value*, Instruction * > InstructionUsed;


    static char ID;
    OurDeadStoreElimination() : FunctionPass(ID) {}

    bool handleStoreOperand(Value *Operand)
    {
        if (StoreVariables.find(Operand) != StoreVariables.end()) {
            if(!StoreVariables[Operand]){
               
                return true;
            }else if(StoreVariables[Operand]){
                return false;
            }
            
        }
        else{

            return false;

        }
  }

    
    void eliminateStoreInstructions(Function &F){

        StoreToRemove.clear();
        StoreVariables.clear();
        InstructionUsed.clear();

        for (BasicBlock &BB : F){
            for (Instruction &I : BB){
                if(isa<StoreInst>(&I)){

                    Value *Operand = I.getOperand(1);
                    bool deleteStore = handleStoreOperand(Operand);

                    if(deleteStore){

                        StoreToRemove.push_back(InstructionUsed[Operand]);
                        auto iter = InstructionUsed.find(Operand);
                        InstructionUsed.erase(iter);
                        InstructionUsed[Operand] = &I;

                    }else{
                        StoreVariables[Operand] = false;
                        InstructionUsed[Operand] = &I;
                    }

                    Value *Operand1 = I.getOperand(0);
                    if (StoreVariables.find(Operand1) != StoreVariables.end()) {
                        StoreVariables[Operand1] = true;
                    }
                    
                }
              
                else if(isa<LoadInst>(&I)){
                    Value *Operand = I.getOperand(0);
                    if (StoreVariables.find(Operand) != StoreVariables.end()) {
                        StoreVariables[Operand] = true;
                    }
                }
                else if (I.getType()->isVoidTy() || isa<CallInst>(&I)) {
                    continue;
                }
                else {
                    for (size_t i = 0; i < I.getNumOperands(); i++) {
                        Value *Operand = I.getOperand(i);
                        if (StoreVariables.find(Operand) != StoreVariables.end()) {
                            StoreVariables[Operand] = true;
                        }
                    }
                }

        }
        }
         // Prolazak kroz sve elemente u StoreVariables
        for (auto& entry : StoreVariables) {
            Value* value = entry.first;
            bool storeToRemove = entry.second;

        // Provera da li treba dodati Instruction u StoreToRemove
            if (!storeToRemove) {
            // Pronalaženje pripadajuće Instruction u InstructionUsed
                auto it = InstructionUsed.find(value);
                if (it != InstructionUsed.end()) {
                    Instruction* instruction = it->second;
     
                    if (std::find(StoreToRemove.begin(), StoreToRemove.end(), instruction) == StoreToRemove.end()) {
                        StoreToRemove.push_back(instruction);
                    }
                }
            }
        }

      

        
        for (Instruction *Instr : StoreToRemove) {
             Instr->eraseFromParent();
        }
        


    }


    bool runOnFunction(Function &F) override {
      
        eliminateStoreInstructions(F);
      
       

        return true;
  }
};
    
}

char OurDeadStoreElimination::ID = 0;
static RegisterPass<OurDeadStoreElimination> X("our-dead-store-elimination", "Our simple constant folding",
                                              false /* Only looks at CFG */,
                                              false /* Analysis Pass */);