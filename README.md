# Dead arg and dead store elimination


## Dead argument elimination
 ./bin/opt -load lib/LLVMOurDeadArgEliminationPass.so -enable-new-pm=0 -dead-arg-elimination ulaz.ll -S -o izlaz.ll

## Dead store elimination 
./bin/opt -load lib/LLVMOurDeadStoreElimination.so -enable-new-pm=0 -our-dead-store-elimination ulaz.ll -S -o izlaz.ll




## Projekat radile:
Marija Papović

Iva Đorđević 

Sandra Petrović

