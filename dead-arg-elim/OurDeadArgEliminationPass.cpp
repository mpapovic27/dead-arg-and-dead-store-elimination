#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace llvm;

namespace {
struct OurDeadArgEliminationPass : public ModulePass {


  std::vector<Function *> NewFunctions;
  std::vector<Function *> FunctionsToRemove;

  std::unordered_map<Value *, Argument *> VariableToArg; 
  std::unordered_map<Value *, bool> UsedArgs;
  std::unordered_map<Value*, Argument*> NewVariablesMap;
  std::vector<Argument *> ArgsToRemove; 
  std::vector<Type *> ArgTypes; 
  std::vector<Instruction *> InstructionsToRemove;
 
  
  bool ArgumentRemoved; 
  
  static char ID;
  OurDeadArgEliminationPass() : ModulePass(ID) {}


   void printRemovedArguments(Function &F) {
    errs() << "Removed arguments on position:\n";
    int i = 0;
    for(Argument &Arg : F.args()) {
      if(std::find(ArgsToRemove.begin(), ArgsToRemove.end(), &Arg) != ArgsToRemove.end()) {
        errs() << "\t" << i << "\n";
      }
      i++;
    }
    if(i == 0) {
      errs() << "None\n";
    }
  }



  Function *createNewFunction(Function &F) {
    LLVMContext &Context = F.getContext();
     std::string NewFuncName = "new_" + F.getName().str();
    
    
    FunctionType *NewFuncType = FunctionType::get(F.getFunctionType()->getReturnType(), ArgTypes, false);
  
    
    Function *NF = Function::Create(NewFuncType,F.getLinkage(), F.getAddressSpace());
    NF->setComdat(F.getComdat());
    F.getParent()->getFunctionList().insert(F.getIterator(), NF);
    NF->takeName(&F);

    //ova funkcija preslikava
    NF->splice(NF->begin(), &F);
    



 Function::arg_iterator NewArgs = NF->arg_begin();
    int r=0;
    for (Function::arg_iterator OldArgs = F.arg_begin(); 
         OldArgs != F.arg_end(); ++OldArgs ) {
          if(UsedArgs[OldArgs]){
            OldArgs->replaceAllUsesWith(NewArgs);
            NewArgs->takeName(OldArgs);
            
            NewArgs++;


          }
          r++;
        
    }
  
   
    
    
    return NF;
}


  void eliminateDeadArguments(Module &M) {


    for(llvm::Function &F : M   ){

    if(F.getName() != "main" && F.getName() != "printf" && std::find(NewFunctions.begin(), NewFunctions.end(), &F) == NewFunctions.end()){
      

    VariableToArg.clear(); 
    UsedArgs.clear();
    ArgsToRemove.clear(); 
    ArgTypes.clear();
    NewVariablesMap.clear();
    InstructionsToRemove.clear();


      errs()<<"Function name: \n";
      errs() << F.getName() << "\n";


    int NumOfArgs = 0;
    for (Argument &Arg : F.args()) {
      Value *value = static_cast<Value *>(&Arg);
      VariableToArg[value] = &Arg;
      UsedArgs[value] = false;
      NewVariablesMap[value] = nullptr;
      NumOfArgs++;
    }

    
    for (BasicBlock &BB : F) {
      for (Instruction &I : BB) {
        if(isa<StoreInst>(&I)) {
          if(NewVariablesMap.find(I.getOperand(0)) != NewVariablesMap.end()) {
            NewVariablesMap[I.getOperand(1)] = VariableToArg[I.getOperand(0)];
          }  
        } 
      }
    }

    
    for (BasicBlock &BB : F) {
      int brojac = 1;
      for (Instruction &I : BB) {
        if(brojac > NumOfArgs*2) {
          for (Use &U : I.operands()) {
            Value *Operand = U.get();
            if(NewVariablesMap.find(Operand) != NewVariablesMap.end()) {
              UsedArgs[NewVariablesMap[Operand]] = true;
            }
          }
        }
        brojac++;
      }
    }

      for (BasicBlock &BB : F) {
        int brojac =0;
      for (Instruction &I : BB) {
        if(brojac < NumOfArgs*2) {
        
        if(isa<StoreInst>(&I)) {
          Value *Operand =  I.getOperand(0);
          if(!UsedArgs[Operand]){
             InstructionsToRemove.push_back(&I);
           
          } 
        } 
      }
      brojac++;
    }
    }

    for (Instruction *Instr : InstructionsToRemove) {
      Instr->eraseFromParent();
     
    }



    
    // Debug ispis koji prikazuje koji argumenti nisu korisceni
    int i = 0;
    for (Argument &Arg : F.args()) {
      Value *value = static_cast<Value *>(&Arg);
      if(UsedArgs[value]) {
        errs() << "Argument: " << i << " is used\n";
      }
      else {
        errs() << "Argument: " << i << " is unused\n";
        ArgsToRemove.push_back(&Arg);
      }
      i++;
    }

    printRemovedArguments(F);
    errs()<< "broj argumenata koji treba ukloniti: " << ArgsToRemove.size() << "\n";
    
    if (!ArgsToRemove.empty()) {
      ArgumentRemoved = true;
      for (Argument &Arg : F.args()) {
        // Dodaj samo argumente koji nisu uklonjeni u listu ArgTypes
        if (std::find(ArgsToRemove.begin(), ArgsToRemove.end(), &Arg) == ArgsToRemove.end()) {
          ArgTypes.push_back(Arg.getType());
        }
      }
    }else{
      for (Argument &Arg : F.args()) {
        // Dodaj samo argumente koji nisu uklonjeni u listu ArgTypes
          ArgTypes.push_back(Arg.getType()); 
      }
      
    }
    errs()<< "broj dobrih argumenata: " << ArgTypes.size() << "\n";
    


      

      Function *NewFunc = createNewFunction(F);
      errs() << "Created new function: " << NewFunc->getName() << "\n";



        
      NewFunctions.push_back(NewFunc);
      F.replaceAllUsesWith(NewFunc);
      FunctionsToRemove.push_back(&F);
      }else{
        continue;
      }



    }
    for (Function *F : FunctionsToRemove) {
        F->eraseFromParent();
    }


   }
      
      

  
  bool runOnModule(Module &M) override {

    eliminateDeadArguments(M);
   
    

    return true;
    
  }
}; 
} 

char OurDeadArgEliminationPass::ID = 0;
static RegisterPass<OurDeadArgEliminationPass> X("dead-arg-elimination", "Eliminate dead arguments from functions",
                                              false /* Only looks at CFG */,
                                              false /* Analysis Pass */);
